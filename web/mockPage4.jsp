<%-- 
    Document   : homepage
    Created on : Mar 19, 2019, 12:31:13 PM
    Author     : MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CharityHound</title>
        <link rel="stylesheet" type="text/css" href="homepage.css">
    </head>
    <body>
        
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        
        <!-- Navigation Bar. Feel free to add in a page I may have forgotten -->
        <div class="navbar">
            <a class="active" href="#home">Home</a>
            <a href="mockPage1.jsp">Charities</a>
            <a href="#contact">Message Board</a>
            <a href="#about">About</a>
        </div>
        
        <h1>Auduborn Nature Institute</h1>
        
        <div id="piechart1"></div>
        
        <div id="piechart2"></div>
        
        <script type="text/javascript">
            google.charts.load('current',{'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            
            function drawChart(){
                var costTo100 = google.visualization.arrayToDataTable([
                    ['100', 'Cost'],
                    ['Spent', 26],
                    ['Cost Per 100', 74]
                ]);
                
                var programPer = google.visualization.arrayToDataTable([
                    ['Program', 'Overhead'],
                    ['Program %', 76],
                    ['Overhead %', 24]
                ]);
                
                var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));
                chart1.draw(costTo100);
                
                var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
                chart2.draw(programPer);
            }
        </script>
        
        <h3 style="text-align: center;">Efficacy Rating: B</h3>
    </body>
</html>
