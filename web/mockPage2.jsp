<%-- 
    Document   : mockPage2
    Created on : Mar 25, 2019, 11:39:39 AM
    Author     : Nik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Charities</title>
        <link rel="stylesheet" type="text/css" href="homepage.css">
    </head>
    <body>
        <div class="navbar">
            <a href="homepage.jsp">Home</a>
            <a class="active" href="mockPage1.jsp">Charities</a>
            <a href="#contact">Message Board</a>
            <a href="#about">About</a>
        </div>
        
        <h1>Charities</h1>
        <form action="mockPage2.jsp" method="get">
            <select>
                <option value="default">Nature</option>
                <input type="submit" value="Submit">
            </select>
            
        </form>
            
        <div class="charList">
            <h2>Audubon Nature Institute</h2> <a href="mockPage4.jsp"><h3>Efficacy Rating: B</h3></a>
            <img src="https://upload.wikimedia.org/wikipedia/en/thumb/f/f8/National_Audubon_Society_logo.png/220px-National_Audubon_Society_logo.png" alt="ADFT logo" height="220" width="190"></img>
            <p>The National Audubon Society protects birds and the places they need, today and tomorrow, throughout the Americas using science, advocacy, education, and on-the-ground conservation.</p>
            <br><a href="mockPage3.jsp"><p>Donate...</p></a>
        </div>
        <br>
        <div class="charList">
            <h2>The Wild Nature Institute</h2> <h3>Efficacy Rating: A</h3>
            <img src="https://www.wildnatureinstitute.org/uploads/5/5/7/7/5577192/published/wni-single-line-logo.jpg?1549001040" alt="ADFT logo" height="80" width="400"></img>
            <p>The Wild Nature Institute Conducts Scientific Research on At-Risk Wildlife Species and Their Habitats, Advocates for Their Protection, and Educates the Public About the Need to Preserve Wild Nature.</p>
            <br><a href="mockPage3.jsp"><p>Donate...</p></a>
        </div>
    </body>
</html>
