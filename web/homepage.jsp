<%-- 
    Document   : homepage
    Created on : Mar 19, 2019, 12:31:13 PM
    Author     : MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CharityHound</title>
        <link rel="stylesheet" type="text/css" href="homepage.css">
    </head>
    <body>
        
        <h1><img src="images/Logo.png" alt="Logo Image" height="200px"></h1>
        
        <!-- Navigation Bar. Feel free to add in a page I may have forgotten -->
        <div class="navbar">
            <a class="active" href="#home">Home</a>
            <a href="mockPage1.jsp">Charities</a>
            <a href="#contact">Message Board</a>
            <a href="#about">About</a>
        </div>
        
        <h2 class="highlight">Today's Highlighted Charity</h2>
        
        <!-- Div here so we can style the highlighted charity -->
        <div class="highChar">
            <h2>The Wild Nature Institute</h2> <h3>Efficacy Rating: A</h3>
            <img src="https://www.wildnatureinstitute.org/uploads/5/5/7/7/5577192/published/wni-single-line-logo.jpg?1549001040" alt="ADFT logo" height="60" width="400"></img>
            <p>The Wild Nature Institute Conducts Scientific Research on At-Risk Wildlife Species and Their Habitats, Advocates for Their Protection, and Educates the Public About the Need to Preserve Wild Nature.</p>
        </div>
        
        <h2>News</h2>
        <div class="homeNews">
            Coming soon...
        </div>
    </body>
</html>
