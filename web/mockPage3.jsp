<%-- 
    Document   : mockPage2
    Created on : Mar 25, 2019, 11:39:39 AM
    Author     : Nik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Charities</title>
        <link rel="stylesheet" type="text/css" href="homepage.css">
    </head>
    <body>
        <h1><img src="images/Logo.png" alt="Logo Image" height="200px"></h1>
        
        <!-- Navigation Bar. Feel free to add in a page I may have forgotten -->
        <div class="navbar">
            <a href="homepage.jsp">Home</a>
            <a href="mockPage1.jsp">Charities</a>
            <a href="#contact">Message Board</a>
            <a href="#about">About</a>
        </div>
        
        <h2>Donation</h2>
        
        <form class="donation" action="homepage.jsp">
            <label for="payment">Payment type: </label>
            <select id="payment">
                <option value="card">Card</option>
                <option value="paypal">PayPal</option>
            </select><br>
            <label for="cardNum">Card Number: </label>
            <input type="text"><br>
            <label for="donationAmount">Donation Amount: </label>
            <input type="text"><br>
            <input type="submit" value="Submit" onclick="alert('Thank you for your donation!')">
        </form>
        </div>
    </body>
</html>
